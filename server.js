const express = require('express');
const report = require('./data/data');
const dotenv = require('dotenv');

const app = express();
dotenv.config();

app.get('/', (req, res) => {
    res.send('api is running');
})

app.get('/api/report', (req, res) => {
    res.json(report);
})

const PORT = process.env.PORT || 5000;

app.listen(5000, console.log(`Server started at port ${PORT}`));
