const auditSection = [{
    "portname": "PDC-WB02",
    "dataandTime": "164B780150",
    "Calibration": [{
            "name": "WP Calibration",
            "booked": "0",
            "present": "0",
            "available": "0"
        },
        {
            "name": "HP Calibration",
            "booked": "1",
            "present": "0",
            "available": "0"
        },
        {
            "name": "BS Calibration",
            "booked": "1",
            "present": "1",
            "available": "0"
        },
        {
            "name": "Detectors Calibration",
            "booked": "0",
            "present": "0",
            "available": "0"
        },
        {
            "name": "Navigation Calibration",
            "booked": "0",
            "present": "0",
            "available": "0"
        },
        {
            "name": "Vextractor position",
            "booked": "1",
            "present": "0",
            "available": "0"
        },
        {
            "name": "Elluminator Vcap",
            "booked": "0",
            "present": "0",
            "available": "0"
        }
    ]
}]

module.exports = auditSection;